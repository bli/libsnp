# Copyright (C) 2023-2024 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Miscellaneous utilities to deal with SNP data."""

import logging
from math import ceil

from cyvcf2 import VCF
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy import __version__ as scipy_version
import seaborn as sns
from seaborn import __version__ as seaborn_version
from sklearn.decomposition import PCA
from sklearn import __version__ as sklearn_version

logging.getLogger(__name__).addHandler(logging.NullHandler())
info = logging.info


# TODO: Maybe should be adapted according to
# the information found in the VCF header.
# def split_annot(annot):
#     """Split an annotation string *annot*."""
#     annot_elems = annot.split("|")
#     gene_id = annot_elems[4]
#     eff_class = annot_elems[1]
#     if eff_class == "missense_variant":
#         eff_mut = "|".join(annot_elems[9:11])
#     else:
#         eff_mut = pd.NA
#     return (gene_id, eff_class, eff_mut)

def make_annot_splitter(vcf, extra_annots=None):
    """
    Return a function that extracts annotation info from an annotation string.

    The information is obtained based on the description of the "ANN" header
    found in the `cyvcf2.VCF` object *vcf*
    """
    annot_names = [
        elem.strip()
        for elem
        in vcf.get_header_type("ANN")["Description"].split("\'")[1].split("|")]

    if extra_annots is None:
        extra_annots = []
    for annot_name in extra_annots:
        msg = f"{annot_name} not among {annot_names}"
        assert annot_name in annot_names, msg

    def split_annot(annot):
        """
        Extract gene identifier, effect class and mutation effect
        from annotation string *annot*.
        """
        annot_dict = dict(zip(annot_names, annot.split("|")))
        gene_id = annot_dict["Gene_ID"]
        eff_class = annot_dict["Annotation"]
        if eff_class == "missense_variant":
            eff_mut = f"{annot_dict['HGVS.c']}|{annot_dict['HGVS.p']}"
        else:
            eff_mut = pd.NA
        return (
            gene_id, eff_class, eff_mut,
            *(annot_dict[annot_name] for annot_name in extra_annots))
    return split_annot


def process_biallelic_variant(rec, annot_splitter):
    """
    Extract information from VCF record *rec* using *annot_splitter* function.

    This assumes the variant is biallelic.
    Otherwise, a `ValueError` might occur.
    """
    ref = rec.REF
    [alt] = rec.ALT
    annot_string = rec.INFO["ANN"]
    variant_tuple = (
        f"{rec.CHROM}_{rec.POS}_{ref}_{alt}",
        rec.CHROM, rec.POS, ref, alt, annot_string,
        *annot_splitter(annot_string))
    # copy is needed, otherwise we end up with
    # absurd values
    alt_count = rec.gt_alt_depths.copy()
    ref_count = rec.gt_ref_depths.copy()
    return {
        "variant_tuple": variant_tuple,
        "alt_count": alt_count,
        "ref_count": ref_count}


# As of 16/04/2024, the only difference with biallelic is the SNP name
def process_multialelic_variant(rec, annot_splitter, do_checks=False):
    """
    Extract information from VCF record *rec* using *annot_splitter* function.

    This assumes the variant is multi-allelic.
    """
    ref = rec.REF
    # alts = rec.ALT
    alt = "_".join(rec.ALT)
    annot_string = rec.INFO["ANN"]
    variant_tuple = (
        f"{rec.CHROM}_{rec.POS}_{ref}_{alt}",
        rec.CHROM, rec.POS, ref, alt, annot_string,
        *annot_splitter(annot_string))
    # Check can fail (https://github.com/brentp/cyvcf2/issues/304)
    if do_checks:
        # https://github.com/brentp/cyvcf2/issues/153#issuecomment-644803246
        # Detailed read depths for all alleles
        # (starting with ref, then, I suppose, in the order of rec.ALT)
        depths = rec.format("AD")
        assert np.alltrue(depths[:, 1:].sum(axis=1) == rec.gt_alt_depths)
    # copy is needed, otherwise we end up with
    # absurd values
    alt_count = rec.gt_alt_depths.copy()
    ref_count = rec.gt_ref_depths.copy()
    return {
        "variant_tuple": variant_tuple,
        "alt_count": alt_count,
        "ref_count": ref_count}


def load_counts_from_joint_vcf(
        vcf_path, chroms,
        extra_headers_dict=None,
        biallelic_only=True,
        snp_only=True,
        allow_mnp=True,
        extra_annots=None):
    """
    Load read counts from a multi-sample .vcf file.

    *vcf_path* should be the path to a .vcf(.gz)
    file containing joint SNP calling data.
    *chroms* should be the list of the chromosome names, in the desired order.
    *extra_headers_dict* should be a dictionary characterizing the data,
    that will be used as column levels on top of the default "RO" and "AO".
    The keys should be level names, and the values level values.

    If *biallelic_only* is set to False, bi_allelic variants will not be
    skipped.
    If *snp_only* is set to False, non-snp variants will not be skipped.
    If *allow_mnp* is set to False, non-indel variants spanning more than
    one base will be skipped.

    *extra_annots* should be an iterable of annotation names as they appear
    in the "Description" part of the "ANN" header of the vcf file.
    Those annotation names will be used to make separate columns in the
    index of the output table.
    "Gene_ID" and "Annotation" will always be included.
    """
    # if not biallelic_only:
    #     raise NotImplementedError("Multi-allelic variants not allowed.")
    if extra_headers_dict is None:
        extra_headers_dict = {}
    vcf_in = VCF(
        vcf_path,
        # strict_gt=True
    )
    samples = vcf_in.samples
    variants = []
    ref_counts = []
    alt_counts = []
    if extra_annots is None:
        extra_annots = []
    split_annot = make_annot_splitter(vcf_in, extra_annots)
    for rec in vcf_in:
        # Some available is_* attributes
        # rec.is_deletion, rec.is_indel
        # rec.is_mnp, rec.is_snp
        # rec.is_sv, rec.is_transition
        not_biallelic = len(rec.ALT) > 1
        if biallelic_only and not_biallelic:
            continue
        if snp_only:
            if not (rec.is_snp or (allow_mnp and rec.is_mnp)):
                # Actually, AC -> AA can be is_mnp and not is_snp
                continue
        # real_indel = rec.is_indel and not rec.is_mnp
        if not_biallelic:
            variant_info = process_multialelic_variant(rec, split_annot)
        else:
            variant_info = process_biallelic_variant(rec, split_annot)
        variants.append(variant_info["variant_tuple"])
        alt_counts.append(variant_info["alt_count"])
        ref_counts.append(variant_info["ref_count"])
        # ref = rec.REF
        # [alt] = rec.ALT
        # annot_string = rec.INFO["ANN"]
        # variants.append((
        #     f"{rec.CHROM}_{rec.POS}_{ref}_{alt}",
        #     rec.CHROM, rec.POS, ref, alt, annot_string,
        #     *split_annot(annot_string)))
        # copy is needed, otherwise we end up with
        # absurd values
        # alt_counts.append(rec.gt_alt_depths.copy())
        # ref_counts.append(rec.gt_ref_depths.copy())
    counts = pd.DataFrame(
        np.concatenate(
            (np.asarray(alt_counts), np.asarray(ref_counts)),
            axis=1),
        columns=pd.MultiIndex.from_tuples(
            [
                (sample, *extra_headers_dict.values(), allele)
                for allele in ["AO", "RO"] for sample in samples],
            names=["sample", *extra_headers_dict.keys(), "count"]),
        index=pd.MultiIndex.from_tuples(
            variants,
            names=[
                "SNP", "chrom", "pos", "ref", "alt",
                "annot",
                "gene_id", "EFF_class", "EFF_mut",
                *extra_annots])).replace(
        to_replace=-1, value=pd.NA)
    chrom_dtype = pd.CategoricalDtype(chroms, ordered=True)
    counts = counts.set_index(
        counts.index.set_levels(
            counts.index.levels[1].astype(chrom_dtype),
            level=1)).sort_index(level=["chrom", "pos"])
    return verbose_drop_has_zero_tot(verbose_dropna(counts)).astype(int)


NOTENOUGH = "(not enough reads in at least one sample to be reliable)"


def verbose_dropna(table):
    """
    Tell how many rows in *table* have missing data in at least one column
    before removing them.
    """
    has_na_in_a_col = table.isna().any(axis=1)
    info(f"{has_na_in_a_col.sum()=}")
    info(f"Dropping rows with NA {NOTENOUGH}")
    return table.dropna()


def verbose_drop_has_zero_tot(counts):
    """
    Tell how many SNPs in counts table *counts* have zero total counts
    in at least one sample before removing them.

    Columns are expected to be MultiIndex with one level named "count".
    Total counts will be computed by summing across this level.
    Other levels could be "sample" and "source". This allows to
    have in the same DataFrame counts from several samples and several
    sources.
    """
    # The groupby is done not only by sample, but also by other
    # levels in the column MultiIndex that are not named "count".
    zero_counts_in_a_sample = (counts.T.groupby(
        level=[
            i for i in range(len(counts.columns.names))
            if i != counts.columns.names.index("count")]
        ).sum().T == 0.0).any(axis=1)
    info(f"{zero_counts_in_a_sample.sum()=}")
    info(f"Dropping SNPs where a sample has zero total counts {NOTENOUGH}")
    return counts.loc[~zero_counts_in_a_sample]


def concat_counts_from_dicts(counts_dict, extra_header_names=None):
    """
    Concatenate counts tables stored in a per-sample dictionary.

    *counts_dicts* should be a dictionary where the keys are sample
    names and the values are pandas DataFrames where lines correspond
    to loci and columns correspond to "RO" and "AO" read counts supporting
    the reference and alternative allele at the corresponding locus for the
    considered sample.
    *extra_header_names* should be column level names to associate
    to the extra headers passed to the *extra_headers* option
    of *load_counts_from_joint_vcf*.
    """
    if extra_header_names is None:
        extra_header_names = tuple()
    # Since SNPs were called on all samples jointly,
    # missing data could be considered as absence of mapped reads.
    # counts = pd.concat(counts_dict, axis=1).fillna(0)
    # However, such loci may rather be considered unreliable:
    # There should be reads for at least one of the alleles
    counts = pd.concat(counts_dict, axis=1)
    info(f"{counts.shape=}")
    counts.columns = counts.columns.set_names(
        ["sample", *extra_header_names, "count"])
    counts = verbose_dropna(counts)
    info(f"{counts.shape=}")
    # remove those lines where a sample has no counts
    counts = verbose_drop_has_zero_tot(counts)
    info(f"{counts.shape=}")
    return counts


def counts2freqs(counts):
    """
    Compute alt allele frequencies from read counts tables.

    *counts* should be a pandas DataFrame where each row represents a locus and
    where columns are a MultiIndex with at least a level named "sample",
    corresponding to the sample name, and one named "count" corresponding to
    the read counts. For each "sample", there should be two values for the
    "count" level: "RO" representing the number of reads supporting the
    reference allele, and "AO" representing the number of reads supporting the
    alt allele.
    """
    freqs = counts.xs("AO", level="count", axis=1) / counts.T.groupby(
        level=[
            i for i in range(len(counts.columns.names))
            if i != counts.columns.names.index("count")]).sum().T
    return freqs


# TODO: make leaf colours configurable
def make_freq_dist_tree(
        freqs,
        method="average",
        orientation="top",
        leaf_rotation=90,
        outdir=None):
    """
    Make a tree representing distances between samples in terms of SNP freqs.

    *freqs* should be a DataFrame containing the allele frequencies as rows and
    the samples as columns.

    *method* is the method used by the linkage function to build the tree.

    *orientation* and *leaf_rotation* can be set to change tree and leaf
    label orientation respectively.
    *outdir*, if provided, should be the path to a directory
    where the plot (in png) and methods (in markdown) should be saved.
    """
    ax = plt.subplot()
    # Intermediate variable could be useful for branch display customization.
    links = linkage(
        freqs.T,
        method=method)
    _ = dendrogram(
        links,
        labels=freqs.columns,
        orientation=orientation,
        leaf_rotation=leaf_rotation,
        color_threshold=-1,
        ax=ax)
    ax.set_ylabel("Distances in the SNP allele frequencies space")
    if method == "average":
        method_name = "UPGMA"
    else:
        method_name = method
    ax.title.set_text(f"{method_name} clustering")
    if outdir is not None:
        plt.savefig(
            outdir.joinpath(f"samples_freq_distance_{method_name}.png"),
            bbox_inches="tight", dpi=300)
        tree_methods = f"""
Euclidean distances between samples in the SNP allele frequencies
space were computed and used to build a dendrogram using the
scipy.cluster.hierarchy module from the SciPy {scipy_version}
([Virtanen _et. al_, 2020][Virt2020]) Python library.
The clustering was obtained using the {method_name} method.

[Virt2020]: <https://doi.org/10.1038/s41592-019-0686-2> (Virtanen P, Gommers R, Oliphant TE, Haberland M, Reddy T, Cournapeau D, et al. SciPy 1.0: Fundamental Algorithms for Scientific Computing in Python. _Nature Methods_. 2020; *17*:261--272)
"""
        methods_path = outdir.joinpath(
            f"samples_freq_distance_{method_name}_methods.md")
        with methods_path.open("w") as fh:
            fh.write(tree_methods)


# TODO: Move do_pca and plot_pca to a separate library.
def do_pca(
        data,
        sample_subset=None,
        transpose=False,
        project_all=False,
        sample_info=None):
    """
    Perform PCA on data *data*.

    Return a tuple containing the corresponding PCA object
    and the data once projected along the principal components.

    *data* should be a pandas DataFrame where columns correspond to
    homogeneous (same nature, readily normalized) variables
    and where rows correspond to samples (observations).

    If *sample_subset* is provided, the PCA is computed using those
    the samples present in this iterable only.

    If *transpose* is set to True, then the data is assumed to have variables
    as rows and observations as columns.

    If *project_all* is set to Trie, then all the data will be projected on
    the principal components, even if a subset of the data was used to compute
    them (using *sample_subset*).

    If provided, *sample_info* should be a table containing information
    about the samples, and have either a column or an index level named
    "sample". This will be used to add this information to the
    transformed data, so as to facilitate its representation
    on PC scatterplots.
    """
    local_data = data.copy()
    if transpose:
        local_data = local_data.T
        data = data.T
    if sample_subset is not None:
        local_data = local_data.loc[sample_subset]
    pca = PCA().fit(local_data)
    if project_all:
        if sample_subset is None:
            # TODO: issue warning
            pass
        else:
            # TODO: add a piece of methods
            pass
        transformed_data = pd.DataFrame(
            pca.transform(data),
            index=data.index)
    else:
        transformed_data = pd.DataFrame(
            pca.transform(local_data),
            index=local_data.index)
    transformed_data.index.name = "sample"
    methods = f"""\
A PCA was performed using the Scikit-learn {sklearn_version}
([Pedregosa _et al._, 2011][sklearn]) Python library.

[sklearn]: <https://scikit-learn.org/stable/about.html>\
(Pedregosa F, Varoquaux G, Gramfort A, Michel V, Thirion B,\
Grisel O, Blondel M, Prettenhofer P, Weiss R, Dubourg V,\
Vanderplas J, Passos A, Cournapeau D, Brucher M, Perrot M and Duchesnay E.\
Scikit-learn: Machine Learning in Python.\
_Journal of Machine Learning_. 2011; **12**:2825--2830)
"""
    if sample_info is not None:
        return (
            pca,
            pd.merge(
                transformed_data,
                sample_info.reset_index().set_index("sample"),
                how="left", left_index=True, right_index=True),
            methods)
    return (pca, transformed_data, methods)


def plot_pca(
        pca, transformed_data,
        nb_pc=8,
        hue=None,
        style=None,
        outdir=None,
        data_name="data",
        pca_methods=""):
    """
    Plot the samples in *transformed_data* accordind to their coordinates
    in the principal components.

    *np_pc* determines the number of principal components that will be used.
    Each pair of successive components will result in one scatterplot.
    Those scatterplots will be arranged on rows containing two columns each.

    *transformed_data* should contain the data projected onto the components,
    samples as rows and components as columns. If *hue* and/or *style* are
    provided, *transformed_data* should also contain columns with those names,
    that will be used to determine the colour and/or shape of the dots
    representing the samples.

    if *outdir* is provided, the plot will be saved in this directory.
    The plot will be saved in a file named '{data_name}_PCA.png', and a methods
    file in markdown format will be written in the same directory as well:
    '{data_name}_PCA_methods.md'.
    *pca_methods* will be prepended to the methods corresponding
    to the plotting.
    """
    # or floor ?
    nrows = ceil(nb_pc / 4)
    (_, axes) = plt.subplots(
        nrows=nrows, ncols=2, figsize=(14, 7 * nrows))

    for (i, ax) in enumerate(axes.flatten()):
        pc1 = 2 * i
        pc2 = pc1 + 1
        # Last requested PC will be plotted against itself if needed
        if pc2 == nb_pc:
            pc2 = pc1
        plot_kwargs = {}
        if hue is not None:
            plot_kwargs["hue"] = hue
        if style is not None:
            plot_kwargs["style"] = style
        sns.scatterplot(
            data=transformed_data, x=pc1, y=pc2,
            alpha=1.0, s=100,
            ax=ax, **plot_kwargs)
        # annotate_outputs(transformed_data, 0, 1, ax)
        part_var_1 = pca.explained_variance_ratio_[pc1]
        part_var_2 = pca.explained_variance_ratio_[pc2]
        ax.set_xlabel(f"PC{pc1} ({part_var_1:.0%} explained variance)")
        ax.set_ylabel(f"PC{pc2} ({part_var_2:.0%} explained variance)")
    if outdir is not None:
        plt.savefig(
            outdir.joinpath(f"{data_name}_PCA.png"),
            dpi=300)
        methods_path = outdir.joinpath(f"{data_name}_PCA_methods.md")
        plot_pca_methods = f"""{pca_methods}
Samples were projected onto the space defined by the principal components,
and represented on PCA plots using the first {nb_pc} principal components.
This was done using the Seaborn {seaborn_version} ([Waskom, 2021][seaborn])
Python library. The percentage of the variance in the data explained by a
given component is reported next to the corresponding axis.

[seaborn] <https://doi.org/10.21105/joss.03021> \
(Waskom ML. Seaborn: Statistical Data Visualization.\
_Journal of Open Source Software_. 2021; **6**:3021)
"""
        with methods_path.open("w") as fh:
            fh.write(plot_pca_methods)


def fst(freqs, pops=None):
    """
    Compute pseudo-Fst from allele frequencies present in DataFrame *freqs*.

    High Fst is obtained when frequencies are heterogeneous between population.
    """
    if pops is None:
        pops = freqs.columns
    freqs = freqs.copy()[pops]
    # frequencies for the meta-pop (mean across populations)
    mean_freqs = freqs.mean(axis=1)
    # heterozygosities of individual pops
    het = 2 * freqs * (1 - freqs)
    h_s = het.mean(axis=1)
    # heterozygosity for the meta-pop
    h_t = 2 * mean_freqs * (1 - mean_freqs)
    # part of the total heterozygosity
    # not due to heterozygosity in sub-pops
    # return ((h_t - h_s) / h_t).dropna().sort_values()
    return (h_t - h_s) / h_t
