from importlib.metadata import version, PackageNotFoundError

try:
    __version__ = version("libsnp")
except PackageNotFoundError:
    # package is not installed
    pass

__copyright__ = "Copyright (C) 2023 Blaise Li"
__licence__ = "GNU GPLv3"
from .libsnp import (
    counts2freqs,
    concat_counts_from_dicts,
    do_pca,
    fst,
    load_counts_from_joint_vcf,
    make_freq_dist_tree,
    plot_pca,
    verbose_dropna,
    verbose_drop_has_zero_tot)
