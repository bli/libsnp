# Miscellaneous utilities to deal with SNP data

This package contains utilities I use to deal with SNP data.


## Installing

Get the source using `git clone git@gitlab.pasteur.fr:bli/libsnp.git`,
`cd` into it and run `python3 -m pip install .`

It might also work directly:

    python3 -m pip install git+ssh://git@gitlab.pasteur.fr/bli/libsnp.git

